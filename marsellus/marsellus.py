import argparse
import itertools
import statistics
import collections
from concurrent.futures import ProcessPoolExecutor
from functools import partial
from typing import Callable, Dict, List, Union
from pathlib import Path

import pandas as pd
import numpy as np
import networkx as nx


mismatch_matrix = collections.namedtuple('mismatch_matrix',
                             ['a', 'b', 'c', 'd', 'n'])

GeneDistances = Dict[str, Dict[str, float]]
ThresholdClusters = Dict[float, nx.DiGraph]
ClustCollapseFunc = Callable[[nx.DiGraph], List[str]]


class ArgumentOutOfRange(Exception):
    pass

def arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--calls',
                        type=Path,
                        required=True,
                        metavar='P',
                        help='Path to tab-delimited typing calls')

    thresholds_help = '''
    The range of thresholds given as a start, stop, and step;
    e.g. from 0.9 to 1.0 in increments of 0.01 would be given as
    `--thresholds 0.9 1.0 0.01`
    '''
    parser.add_argument('-t', '--thresholds',
                        nargs=3,
                        type=float,
                        metavar=('START', 'STOP', 'STEP'),
                        help=thresholds_help)

    parser.add_argument('-j', '--cores',
                        type=int,
                        default=1,
                        metavar='N',
                        help='CPU cores to use [1]')

    args = parser.parse_args()

    if args.thresholds[0] < 0:
        raise ArgumentOutOfRange('Minimum threshold is below 0.0')

    if args.thresholds[1] > 1:
        raise ArgumentOutOfRange('Maximum threshold is above 1.0')

    return args


def contingency_table(calls: pd.DataFrame,
                      partition_a: str, partition_b: str) -> pd.DataFrame:

    ct = pd.crosstab(calls[partition_a], calls[partition_b])

    return ct


def mismatch(ct: pd.DataFrame) -> mismatch_matrix:

    def f(x: Union[pd.DataFrame, pd.Series]) -> np.float64:
        return (x * (x - 1)).values.sum() / 2

    col_sums = ct.sum(0)
    row_sums = ct.sum(1)

    n = sum(col_sums)

    a = f(ct)

    b = f(col_sums) - a

    c = f(row_sums) - a

    d = ((n * (n - 1)) / 2) - (b + a) - c

    return mismatch_matrix(a, b, c, d, n)


def simpsons(classifications: pd.Series) -> float:

    def n_i(i) -> int:
        return sum(classifications == i)

    n = len(classifications)
    s = set(classifications)

    return 1 - (sum(n_i(i) * (n_i(i) - 1) for i in s) / (n * (n - 1)))


def wallace(mismatches: mismatch_matrix) -> float:

    # return mismatches.a / (mismatches.a + mismatches.b)

    # This returns wallace B -> A, which doesn't match what
    # Comparing Partitions says, but matches what Comparing Partitions does
    return mismatches.a / (mismatches.a + mismatches.c)


def adj_wallace(calls: pd.DataFrame,
                partition_a: str, partition_b: str) -> float:

    ct = contingency_table(calls, partition_a, partition_b)

    mismatches = mismatch(ct)

    sid_b = simpsons(calls[partition_b])

    # wallace_a_b = wallace(mismatches)

    # wallace_a_b = mismatches.a / (mismatches.a + mismatches.b)
    wallace_b_a = wallace(mismatches)

    wallace_i = 1 - sid_b

    # awc_using_wallace_a_b = (wallace_a_b - wallace_i) / (1 - wallace_i)
    awc_using_wallace_b_a = (wallace_b_a - wallace_i) / (1 - wallace_i)

    return awc_using_wallace_b_a  # mimics the behaviour of Comparing Partitions


def pairwise_compare(calls: pd.DataFrame, cores: int) -> GeneDistances:

    results = {}

    gene_pairs = list(itertools.permutations(calls.columns, r=2))

    awc = partial(adj_wallace, calls)

    # Submit jobs
    with ProcessPoolExecutor(max_workers=cores) as ppe:
        wallace_results = ppe.map(awc, *zip(*gene_pairs),
                                  chunksize=len(calls.columns))

    # Organize results
    for a_b, result in zip(gene_pairs, wallace_results):
        a, b = a_b
        try:
            results[a][b] = result

        except KeyError:
            results[a] = {b: result}

    return results


def average_pairwise(gene_pairs: GeneDistances) -> pd.DataFrame:

    averages = {}
    for a, b in itertools.permutations(gene_pairs, r=2):

        result = statistics.mean((gene_pairs[a][b], gene_pairs[b][a]))

        try:
            averages[a][b] = result

        except KeyError:
            averages[a] = {b: result}

    return pd.DataFrame(averages).fillna(1.0)


def create_linkage_map(gene_distances: GeneDistances) -> nx.DiGraph:

    linkage_graph = nx.DiGraph()

    for a, b in itertools.permutations(gene_distances.keys(), r=2):

        linkage_graph.add_edge(a, b, awc=gene_distances[a][b])

    return linkage_graph


def cluster_graph(linkage_graph: nx.DiGraph, threshold: float) -> nx.DiGraph:

    links: nx.DiGraph = linkage_graph.copy()

    under_threshold = [(u, v)
                       for u, v, awc
                       in links.edges.data('awc')
                       if awc < threshold]

    links.remove_edges_from(under_threshold)

    return links


def distill(linkage_graph: nx.DiGraph,
            thresholds: List[float]) -> ThresholdClusters:

    return {threshold: cluster_graph(linkage_graph, threshold)
            for threshold in thresholds}


def collapse_clusters(clusters: nx.DiGraph,
                      collapsing_method: ClustCollapseFunc) -> List[List[str]]:

    cluster_subgraphs = nx.strongly_connected_component_subgraphs(clusters)

    return [collapsing_method(cluster) for cluster in cluster_subgraphs]


def weight_count(cluster: nx.DiGraph) -> List[str]:
    # Hypothesis 1

    outbound_weight = collections.defaultdict(float)

    for u, v, awc in cluster.edges.data('awc'):
        outbound_weight[u] += awc
        outbound_weight[v] -= awc

    return [n for n in outbound_weight.keys()
            if outbound_weight[n] == max(outbound_weight.values())]


def edge_count(cluster: nx.DiGraph) -> List[str]:
    # Hypothesis 2

    outbound_count = collections.defaultdict(int)

    for node, outbound in cluster.adjacency():

        outbound_edge_count = len(outbound)

        outbound_count[node] += outbound_edge_count

        for connected_node in outbound:

            outbound_count[connected_node] -= 1

    return [n for n in outbound_count.keys()
            if outbound_count[n] == max(outbound_count.values())]


def cluster_centre(cluster: nx.DiGraph) -> List[str]:
    # Hypothesis 3

    return nx.center(cluster)


def compare_methods(linkage_map: nx.DiGraph, thresholds: List[float]):

    thresholds = distill(linkage_map, thresholds)

    results = {}

    for threshold, clusters in thresholds.items():

        results[threshold] = {}

        for method in (weight_count, edge_count, cluster_centre):

            collapsed = collapse_clusters(clusters, method)
            results[threshold][method.__name__] = collapsed


    for threshold, v1 in results.items():
        for method, v2 in v1.items():

            name = f'{threshold}_{method}.txt'
            Path(name).write_text(v2)

def main():

    args = arguments()

    calls = pd.read_csv(args.calls, sep=',', header=0, index_col=0)

    gene_similarity = pairwise_compare(calls, args.cores)

    linkage_map = create_linkage_map(gene_similarity)

    comparison = compare_methods(linkage_map, args.thresholds)



if __name__ == '__main__':
    main()
