import os
import sys
sys.path.insert(0, os.path.abspath('..'))

import pandas as pd

import unittest
from marsellus import marsellus


class WallaceTests(unittest.TestCase):

    def setUp(self):

        self.test_data = pd.read_csv('test_data.csv', sep=',', header=0)

    def test_wallace(self):

        result = marsellus.adj_wallace(self.test_data, 'T type', 'emm type')

        self.assertEqual(round(result, 3), 0.608)

class BuildResultsTest(unittest.TestCase):

    def setUp(self):

        self.data = {
            'a': {
                'b': 2.0,
                'c': 3.0
            },
            'b': {
                'a': 3.0,
                'c': 1.0
            },
            'c': {
                'a': 1.0,
                'b': 2.0
            }
        }

        self.result_df = pd.DataFrame({'a': [1.0, 2.5, 2.0],
                                       'b': [2.5, 1.0, 1.5],
                                       'c': [2.0, 1.5, 1.0]},
                                      index=['a', 'b', 'c'])

    def test_bidirectional_awc_dataframe(self):

        result = marsellus.average_pairwise(self.data)

        self.assertTrue(all(result == self.result_df))


class GeneClusteringTest(unittest.TestCase):

    def setUp(self):

        self.data = pd.DataFrame({'a': [1, 1, 2],
                                  'b': [2, 2, 2],
                                  'c': [3, 4, 3]},
                                 index=['x', 'y', 'z'])

    def test_clustering(self):
        pass


if __name__ == '__main__':
    unittest.main()
